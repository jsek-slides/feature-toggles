import Vue from 'vue'
import './plugins/prism'
import './plugins/eagle'
import './plugins/vuetify'

import App from './App.vue'

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
