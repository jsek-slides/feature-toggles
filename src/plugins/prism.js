import Vue from 'vue'
import VuePrism from 'vue-prism'
import 'prismjs/themes/prism-tomorrow.css';
import 'prismjs/components/prism-csharp';

Vue.use(VuePrism)
