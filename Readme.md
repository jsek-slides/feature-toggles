# Presentation about Feature Toggles

Live at [feature-toggles.surge.sh](http://feature-toggles.surge.sh/)

## Setup

```
npm install
```

## Run

Recommended: with Vue UI

or

```
npm start
```

## Resources

- [launchdarkly](https://launchdarkly.com/feature-management/)
- [featureflags.io](http://featureflags.io/)

## Take away

- switch pointers
- merge more often
- releases != deployments
- cleanup ASAP